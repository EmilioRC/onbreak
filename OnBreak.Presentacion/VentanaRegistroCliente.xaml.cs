﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using OnBreak.Modelo;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace OnBreak.Presentacion
{
    /// <summary>
    /// </summary>
    public partial class VentanaRegistroCliente : MetroWindow
    {

        #region Propiedades

        protected ClienteCollection MisClientes { get; set; }

        #endregion Propiedades

        #region Constructores

        public VentanaRegistroCliente()
        {
            InitializeComponent();

            MisClientes = new ClienteCollection();
            
            MisClientes.Add(new Cliente("21433671-5", "Coca-Cola Company", "San Vicente 123, La Florida", "+56 9 2134 2343", ActividadEmpresa.Comercio, TipoEmpresa.SPA));
            MisClientes.Add(new Cliente("23416800-2", "Zapatillas Monstruo", "Pedro Aguirre Cerda 2536, Estación Central", "+56 9 2134 2343", ActividadEmpresa.Comercio, TipoEmpresa.SPA));
            MisClientes.Add(new Cliente("13534315-3", "Energética Monster", "Alameda 2121, Santiago", "+56 9 2134 2343", ActividadEmpresa.Comercio, TipoEmpresa.SPA));
            MisClientes.Add(new Cliente("11470332-K", "Compañía Zapatillas Puma", "Calle Los Alerces 637, La Reina", "+56 9 2134 2343", ActividadEmpresa.Comercio, TipoEmpresa.SPA));

            RefrescarDataGrid();
        }

        #endregion Constructores

        #region Eventos

        private void BtnBuscar_Click(object sender, RoutedEventArgs e)
        {
            ConsultarPorRut();
        }

        private void BtnGuardar_Click(object sender, RoutedEventArgs e)
        {
            GuardarCliente();
        }

        private void BtnLimpiar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarVentana();
        }

        private void BtnEliminar_Click(object sender, RoutedEventArgs e)
        {
            EliminarCliente();
        }

        private void BtnConsultarCliente_Click(object sender, RoutedEventArgs e)
        {
            ConsultarCliente();
        }

        private void BtnSalir_Click(object sender, RoutedEventArgs e)
        {
            CerrarVentana();
        }

        private void DgrClientes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SeleccionarCliente();
        }
        
        #endregion Eventos

        #region Métodos

        private void RefrescarDataGrid()
        {
            dgrClientes.ItemsSource = MisClientes;
            dgrClientes.Items.Refresh();
        }

        private Cliente ObtenerCliente()
        {
            TipoEmpresa tipoEmp = TipoEmpresa.SPA;
            ActividadEmpresa actEmp = ActividadEmpresa.Agropecuaria;
            if (cmbTipoE.Text == "SPA") tipoEmp = TipoEmpresa.SPA;
            if (cmbTipoE.Text == "EIRL") tipoEmp = TipoEmpresa.EIRL;
            if (cmbTipoE.Text == "Limitada") tipoEmp = TipoEmpresa.Limitada;
            if (cmbTipoE.Text == "Sociedad Anonima") tipoEmp = TipoEmpresa.SociedadAnonima;

            if (cmbActEmp.Text == "Agropecuaria") actEmp = ActividadEmpresa.Agropecuaria;
            if (cmbActEmp.Text == "Mineria") actEmp = ActividadEmpresa.Mineria;
            if (cmbActEmp.Text == "Manufactura") actEmp = ActividadEmpresa.Manufactura;
            if (cmbActEmp.Text == "Comercio") actEmp = ActividadEmpresa.Comercio;
            if (cmbActEmp.Text == "Hoteleria") actEmp = ActividadEmpresa.Hoteleria;
            if (cmbActEmp.Text == "Alimentos") actEmp = ActividadEmpresa.Alimentos;
            if (cmbActEmp.Text == "Transporte") actEmp = ActividadEmpresa.Transporte;
            if (cmbActEmp.Text == "Servicios") actEmp = ActividadEmpresa.Servicios;

            return new Cliente(txtRut.Text, txtRazonS.Text, txtDireccion.Text, txtTelefono.Text, actEmp, tipoEmp);
        }

        private async void ConsultarPorRut()
        {
            try
            {
                Cliente cli = MisClientes.BuscarPorRut(txtRut.Text);

                if (cli == null)
                {
                    await this.ShowMessageAsync("Mensaje: ", string.Format("El cliente con el rut {0} no existe en la Base de Datos.", txtRut.Text));
                    LimpiarVentana();
                    return;
                }

                LlenarCliente(cli);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GuardarCliente()
        {

            try
            {
                String rut = txtRut.Text;
                Cliente cli = MisClientes.BuscarPorRut(rut);

                if(cli == null)
                {
                    AgregarCliente();
                }
                else
                {
                    ModificarCliente(cli);
                }

                RefrescarDataGrid();
            
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AgregarCliente()
        {
            MisClientes.Add(ObtenerCliente());
        }

        private void ModificarCliente(Cliente clienteAModificar)
        {
            Cliente clienteEnPantalla = ObtenerCliente();
            clienteAModificar.Rut = clienteEnPantalla.Rut;
            clienteAModificar.RazonSocial = clienteEnPantalla.RazonSocial;
            clienteAModificar.Direccion = clienteEnPantalla.Direccion;
            clienteAModificar.Telefono = clienteEnPantalla.Telefono;
            clienteAModificar.Tipo = clienteEnPantalla.Tipo;
            clienteAModificar.Actividad = clienteEnPantalla.Actividad;
        }

        public void LimpiarVentana()
        {
            txtRut.Text = "";
            txtRazonS.Text = "";
            txtDireccion.Text = "";
            txtTelefono.Text = "";
            cmbActEmp.SelectedIndex = -1;
            cmbTipoE.SelectedIndex = -1;
        }

        private async void EliminarCliente()
        {
            String rut = txtRut.Text;
            if(String.IsNullOrEmpty(rut))
            {
                await this.ShowMessageAsync("Mensaje: ", "Para eliminar un cliente debe seleccionar un rut");
                return;
            }
            bool eliminado = MisClientes.EliminarCliente(rut);
            RefrescarDataGrid();
            if(eliminado)
            {
                await this.ShowMessageAsync("Mensaje: ", "El cliente fue eliminado Correctamente");
                LimpiarVentana();
            }
            else
            {
                await this.ShowMessageAsync("Mensaje: ", string.Format("No se encontro un cliente con el rut {0}",rut));
            }
        }

        private void ConsultarCliente()
        {
            VentanaConsultaCliente win = new VentanaConsultaCliente(MisClientes.ObtenerClon());
            win.ShowDialog();

            if (win.ClienteSeleccionado != null)
            {
                LlenarCliente(win.ClienteSeleccionado);
                dgrClientes.ItemsSource = new ClienteCollection();
                dgrClientes.Items.Refresh();
            }

            RefrescarDataGrid();
        }

        private void CerrarVentana()
        {
            this.Close();
        }

        private void LlenarCliente(Cliente cli)
        {
            txtRut.Text = cli.Rut;
            txtDireccion.Text = cli.Direccion;
            txtRazonS.Text = cli.RazonSocial;
            txtTelefono.Text = cli.Telefono;

            if (cli.Tipo == TipoEmpresa.SPA) cmbTipoE.Text = "SPA";
            if (cli.Tipo == TipoEmpresa.EIRL) cmbTipoE.Text = "EIRL";
            if (cli.Tipo == TipoEmpresa.Limitada) cmbTipoE.Text = "Limitada";
            if (cli.Tipo == TipoEmpresa.SociedadAnonima) cmbTipoE.Text = "Sociedad Anonima";

            if (cli.Actividad == ActividadEmpresa.Agropecuaria) cmbActEmp.Text = "Agropecuaria";
            if (cli.Actividad == ActividadEmpresa.Mineria) cmbActEmp.Text = "Mineria";
            if (cli.Actividad == ActividadEmpresa.Manufactura) cmbActEmp.Text = "Manufactura";
            if (cli.Actividad == ActividadEmpresa.Comercio) cmbActEmp.Text = "Comercio";
            if (cli.Actividad == ActividadEmpresa.Hoteleria) cmbActEmp.Text = "Hoteleria";
            if (cli.Actividad == ActividadEmpresa.Alimentos) cmbActEmp.Text = "Alimentos";
            if (cli.Actividad == ActividadEmpresa.Transporte) cmbActEmp.Text = "Transporte";
            if (cli.Actividad == ActividadEmpresa.Servicios) cmbActEmp.Text = "Servicios";


        }

        private void SeleccionarCliente()
        {
            if (dgrClientes.SelectedValue != null)
                LlenarCliente((Cliente)dgrClientes.SelectedValue);
        }

        #endregion Métodos
        
        
    }
}