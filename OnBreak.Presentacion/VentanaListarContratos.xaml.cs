﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using OnBreak.Modelo;
using MahApps.Metro.Controls;

namespace OnBreak.Presentacion
{
    /// <summary>
    /// Interaction logic for VentanaListarContratos.xaml
    /// </summary>
    public partial class VentanaListarContratos : MetroWindow
    {
        #region Propiedades
            private ContratoCollection MisContratos { get; set; }

            private ContratoCollection MisContratosEnElDataGrid { get; set; }

            public  Contrato ContratoSeleccionado{ get; set; }
        #endregion Propiedades

        #region Constructor
        public VentanaListarContratos(ContratoCollection misContratos)
        {
            InitializeComponent();

            this.MisContratos = misContratos;
            this.MisContratosEnElDataGrid = new ContratoCollection();
            this.ContratoSeleccionado = null;
        }
        #endregion Constructor

        #region Eventos
        private void BtnBuscar_Click(object sender, RoutedEventArgs e)
        {
            BuscarContrato();
        }
        
        #endregion Eventos

        #region Metodo
        private void RefrescarDataGrid()
        {
            dgrContratos.ItemsSource = MisContratosEnElDataGrid;
            dgrContratos.Items.Refresh();
        }
        
        public void BuscarContrato()
        {
            bool numeroDeContrato = !String.IsNullOrEmpty(txtNumeroContrato.Text);
            bool rutFueDigitado = !String.IsNullOrEmpty(txtRut.Text);

            int numContrato;
            if(txtNumeroContrato.Text=="")
            {
                numContrato = 0;
            }
            else
            {
                numContrato = int.Parse(txtNumeroContrato.Text);
            }
            String rut = txtRut.Text;
            String tipo;
            if (cmbTipoContrato.SelectedIndex == 1)
            {
                tipo = "PaseoFinAño";
            }
            else
            {
                tipo = cmbTipoContrato.Text;
            }
            

            if (numeroDeContrato)
            {
                MostrarNumeroEncontradoEnDataGrid(numContrato);
                return;
            }

            if (rutFueDigitado)
            {
                MostrarClientesEncontradosEnDataGridRut(rut);
                return;
            }

            if (cmbTipoContrato.SelectedIndex != -1)
            {
                MostrarClientesEncontradosEnDataGridTipo(tipo);
                return;
            }

            MessageBox.Show("Para realizar una búsqueda debe escribir algún criterio en el formulario");
        }

        private void MostrarNumeroEncontradoEnDataGrid(int numero)
        {
            Contrato contrato = MisContratos.BuscarPorContrato(numero);

            if (contrato == null)
            {
                MessageBox.Show("No se encontró un contrato con el número " + numero);
                MisContratosEnElDataGrid.Clear();
                RefrescarDataGrid();
                return;
            }

            MisContratosEnElDataGrid.Clear();
            MisContratosEnElDataGrid.Add(contrato);
            RefrescarDataGrid();
        }

        private void MostrarClientesEncontradosEnDataGridRut(String rut)
        {
            ContratoCollection contratos = MisContratos.BuscarContratoPorRut(rut);

            if (contratos == null)
            {
                MessageBox.Show("No se encontró un contrato con el rut " + rut);
                MisContratosEnElDataGrid.Clear();
                RefrescarDataGrid();
                return;
            }

            MisContratosEnElDataGrid.Clear();
            MisContratosEnElDataGrid = contratos;
            RefrescarDataGrid();
        }

        private void MostrarClientesEncontradosEnDataGridTipo(String tipo)
        {
            ContratoCollection contratos = MisContratos.BuscarTipo(tipo);

            if (contratos == null)
            {
                MessageBox.Show("No se encontró un contrato con el tipo " + tipo);
                MisContratosEnElDataGrid.Clear();
                RefrescarDataGrid();
                return;
            }

            MisContratosEnElDataGrid.Clear();
            MisContratosEnElDataGrid = contratos;
            RefrescarDataGrid();
        }
        #endregion Metodo
    }
}
