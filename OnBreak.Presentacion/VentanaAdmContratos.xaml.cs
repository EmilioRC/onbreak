﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using OnBreak.Modelo;
using MahApps.Metro.Controls;

namespace OnBreak.Presentacion
{
    /// <summary>
    /// Interaction logic for VentanaAdmContratos.xaml
    /// </summary>
    public partial class VentanaAdmContratos : MetroWindow
    {

        #region Propiedades

        protected ContratoCollection MisContratos { get; set; }

        #endregion

        #region Constructores
        public VentanaAdmContratos()
        {
            InitializeComponent();

            MisContratos = new ContratoCollection();
            MisContratos.Add(new Contrato(123, new Cliente("21433671-5", "Coca-Cola Company", "San Vicente 123, La Florida", "+56 9 2134 2343", ActividadEmpresa.Comercio, TipoEmpresa.SPA)
                , new DateTime(1950, 12, 03), new DateTime(1950, 12, 03), "13:04:09", "15:04:10", "Los Heroes", true, TipoContrato.Aniversario, "Holi", 10, 15));


            //RefrescarDataGrid();
        }
        #endregion

        #region Eventos



        private void btnBuscarPorContrato_Click(object sender, RoutedEventArgs e)
        {
            ConsultarPorContrato();
        }

        private void btnConsultarPorRutCLiente_Click(object sender, RoutedEventArgs e)
        {
            ConsultarPorRutCliente();
        }

        private void btnGuardarContacto_Click(object sender, RoutedEventArgs e)
        {
            GuardarContrato();
        }

        private void btnActualizarContrato_Click(object sender, RoutedEventArgs e)
        {
            ActualizarContrato();
        }

        private void btnBuscarContrato_Click(object sender, RoutedEventArgs e)
        {
            BuscarContrato();
        }

        private void btnBuscarCliente_Click(object sender, RoutedEventArgs e)
        {
            BuscarCliente();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Cancelar();
        }

        #endregion

        #region Metodos

        private Contrato ObtenerContrato()
        {
            TipoContrato tipoCon = TipoContrato.Aniversario;

            if (cmbTipoEvento.Text == "Aniversario") tipoCon = TipoContrato.Aniversario;
            if (cmbTipoEvento.Text == "PaseoFinAño") tipoCon = TipoContrato.PaseoFinAño;
            if (cmbTipoEvento.Text == "Seminario") tipoCon = TipoContrato.Seminario;

            return new Contrato(int.Parse(txtNumContrato.Text), new Cliente("21433671-5", "Coca-Cola Company", "San Vicente 123, La Florida", "+56 9 2134 2343", ActividadEmpresa.Comercio, TipoEmpresa.SPA)
                , dpFechaInicioEvento.SelectedDate.Value, dpFechaTerminoEvento.SelectedDate.Value, txtHoraInicio.Text, txtHoraFin.Text, txtDireccionEvento.Text, true, tipoCon, txtObservaciones.Text, int.Parse(txtCantidadAsistentes.Text), int.Parse(txtCantidadPersonal.Text));
        }

        private void ConsultarPorContrato()
        {
            try
            {
                Contrato con = MisContratos.BuscarPorContrato(int.Parse(txtNumContrato.Text));
                if (con == null)
                {
                    MessageBox.Show("EL numero del contrato " + txtNumContrato.Text + " no existe en la Base de Datos.");
                    LimpiarVentana();
                    return;
                }
                LLenarContrato(con);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ConsultarPorRutCliente()
        {
            try
            {
                
                Contrato con = MisContratos.BuscarPorRut(txtRutCliente.Text);
                
                if (con == null)
                {
                    MessageBox.Show("El cliente con el rut " + txtRutCliente.Text + " no existe en la base de datos.");
                    LimpiarVentana();
                    return;
                }
                LLenarContrato(con);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GuardarContrato()
        {
            try
            {
                
                int NumeroContrato = int.Parse(txtNumContrato.Text);
                Contrato con = MisContratos.BuscarPorContrato(NumeroContrato);

                if (con == null)
                {
                    AgregarContrato();
                }
                else
                {
                    ModificarContrato(con);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AgregarContrato()
        {
            MisContratos.Add(ObtenerContrato());
        }

        private void ModificarContrato(Contrato contratoAModificar)
        {
            Contrato contratoEnPantalla = ObtenerContrato();
            contratoAModificar.NumeroContrato = contratoEnPantalla.NumeroContrato;
            contratoAModificar.Cliente.Rut = contratoEnPantalla.Cliente.Rut;
            contratoAModificar.Cliente.RazonSocial = contratoEnPantalla.Cliente.RazonSocial;
            contratoAModificar.Cliente.Tipo = contratoEnPantalla.Cliente.Tipo;
            contratoAModificar.Tipo = contratoEnPantalla.Tipo;
            contratoAModificar.CantidadAsistentes = contratoEnPantalla.CantidadAsistentes;
            contratoAModificar.Creacion = contratoEnPantalla.Creacion;
            contratoAModificar.Termino = contratoEnPantalla.Termino;
            contratoAModificar.Direccion = contratoEnPantalla.Direccion;
            contratoAModificar.Cliente.Actividad = contratoEnPantalla.Cliente.Actividad;
            contratoAModificar.FechaHoraInicio = contratoEnPantalla.FechaHoraInicio;
            contratoAModificar.FechaHoraTermino = contratoEnPantalla.FechaHoraTermino;
            contratoAModificar.Observacion = contratoEnPantalla.Observacion;
        }

        private void ActualizarContrato()
        {

        }
        private void BuscarContrato()
        {
            VentanaListarContratos win = new VentanaListarContratos(MisContratos.ObtenerClon());
            win.ShowDialog();

            if (win.ContratoSeleccionado != null)
            {
                LLenarContrato(win.ContratoSeleccionado);
            }
            
        }

        private void BuscarCliente()
        {
            VentanaRegistroCliente win = new VentanaRegistroCliente();
            win.ShowDialog();

        }

        private void Cancelar()
        {
            this.Close();
        }

        private void CalcularValorTotal(int valorBase)
        {
            double Uf = 27551.56F;
            int cantAsistentes = 0;
            int cantPersonal = 0;
            double valorTotal = 0;
            double recargoAsis = 0;
            double recargoPersonal = 0;

            if (cantAsistentes>=1 | cantAsistentes <=20)
            {
                recargoAsis = Uf * 3;
            }
            if(cantAsistentes >= 21 | cantAsistentes <= 50)
            {
                recargoAsis = Uf * 5;
            }
            if (cantAsistentes > 50)
            {
                recargoAsis = Uf * 5;
                double tempoval = 0;
                tempoval = (cantAsistentes - 50) / 20;
                tempoval = Math.Truncate(tempoval);
                recargoAsis = tempoval + recargoAsis ;
            }

            if (cantPersonal > 1 || cantPersonal <= 3)
            {
                recargoPersonal = Uf * cantPersonal;
            }
            if (cantPersonal > 3)
            {
                recargoPersonal = (cantPersonal-3)*0.5;  // Uf Adicional +4 personas
                recargoPersonal = recargoPersonal + 3;
                recargoPersonal = recargoPersonal * Uf; // Calculo Valor Recargo Personal
            }

            valorTotal = valorBase + recargoAsis + recargoPersonal;
            valorTotal = Math.Truncate(valorTotal);
            txtValorTotal.Text = valorTotal.ToString();
            
        }

        private void LLenarContrato(Contrato con)
        {
            txtNumContrato.Text = Convert.ToString(con.NumeroContrato);
            txtRutCliente.Text = con.Cliente.Rut;
            txtRazonSocial.Text = con.Cliente.RazonSocial;
            txtTipoEmpresa.Text = Convert.ToString(con.Cliente.Tipo);
            txtCantidadAsistentes.Text = Convert.ToString(con.CantidadAsistentes);
            dpFechaInicioEvento.SelectedDate = con.Creacion;
            dpFechaTerminoEvento.SelectedDate = con.Termino;
            txtDireccionEvento.Text = con.Direccion;
            //txtNombreContacto.Text = con.Cliente.
            txtActividadComercial.Text = Convert.ToString(con.Cliente.Actividad);
            txtCantidadPersonal.Text = Convert.ToString(con.CantidadPersonal);
            txtHoraInicio.Text = con.FechaHoraInicio;
            txtHoraFin.Text = con.FechaHoraTermino;
            txtObservaciones.Text = con.Observacion;
            

            if (con.Tipo == TipoContrato.Aniversario)
            {
                cmbTipoEvento.Text = "Aniversario";
                txtValorBase.Text = "1000000";
                CalcularValorTotal(1000000);
            }
            if (con.Tipo == TipoContrato.PaseoFinAño)
            {
                cmbTipoEvento.Text = "Paseo Fin Año";
                txtValorBase.Text = "2000000";
                CalcularValorTotal(2000000);
            }
            if (con.Tipo == TipoContrato.Seminario)
            {
                cmbTipoEvento.Text = "Seminario";
                txtValorBase.Text = "3000000";
                CalcularValorTotal(3000000);
            }

        }

        public void LimpiarVentana()
        {
            txtCreado.Text = "";
            txtEstado.Text = "";
            txtFechaTermino.Text = "";
            txtNumContrato.Text = "";
            txtRutCliente.Text = "";
            txtRazonSocial.Text = "";
            txtTipoEmpresa.Text = "";
            cmbTipoEvento.SelectedIndex = -1;
            txtCantidadAsistentes.Text = "";
            txtValorTotal.Text = "";
            //txtFechaInicioEvento.Text = "";
            //txtFechaFinEvento.Text = "";
            //txbDescripcionEvento.Text = "";
            txtNombreContacto.Text = "";
            txtActividadComercial.Text = "";
            txtValorBase.Text = "";
            txtCantidadPersonal.Text = "";
            //txtHoraInicio.Text = "";
            //txtHoraFin.Text = "";
            //txtObservaciones.Text = "";
        }

        #endregion

    }
}
