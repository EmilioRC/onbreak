﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnBreak.Modelo
{
    public class ContratoCollection : List<Contrato>
    {
        #region Constructores
        public ContratoCollection()
        {

        }

        public ContratoCollection(Contrato contrato)
        {
            this.Add(contrato);
        }
        #endregion

        #region Metodos
        public Contrato BuscarPorContrato(int contrato)
        {
            return this.Where(r => r.NumeroContrato == contrato).FirstOrDefault();
        }

        public Contrato BuscarPorRut(string rut)
        {
            return this.Where(r => r.Cliente.Rut == rut).FirstOrDefault();
        }

        public ContratoCollection BuscarContratoPorRut(String rut)
        {
            List<Contrato> lista = this.Where(r => r.Cliente.Rut == rut).ToList();

            ContratoCollection contratos = new ContratoCollection();

            foreach (Contrato contrato in lista)
            {
                contratos.Add(contrato);
            }
            if (contratos.Count == 0) return null;
            return contratos;
        }

        public ContratoCollection BuscarTipo(String tipo)
        {
            tipo = tipo.ToUpper();
            List<Contrato> lista = this.Where(r => r.Tipo.ToString().ToUpper().Contains(tipo)).ToList();

            ContratoCollection contratos = new ContratoCollection();

            foreach (Contrato contrato in lista)
            {
                contratos.Add(contrato);
            }
            if (contratos.Count == 0) return null;
            return contratos;
        }

        public ContratoCollection ObtenerClon()
        {
            // Implementar...
            ContratoCollection clonContratos = new ContratoCollection();

            foreach (Contrato con in this)
            {
                Contrato clon = new Contrato(con.NumeroContrato, con.Cliente, con.Creacion, con.Termino, con.FechaHoraInicio, con.FechaHoraTermino,
                    con.Direccion, con.EstaVigente, con.Tipo, con.Observacion, con.CantidadAsistentes, con.CantidadPersonal);
                clonContratos.Add(clon);
            }
            return clonContratos;
        }
        #endregion
    }

}
