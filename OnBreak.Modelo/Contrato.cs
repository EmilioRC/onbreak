﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnBreak.Modelo
{
    public class Contrato
    {
        #region Campos

        private int _numeroContrato;
        private Cliente _cliente;
        private DateTime _creacion;
        private DateTime _termino;
        private String _fechaHoraInicio;
        private String _fechaHoraTermino;
        private String _direccion;
        private Boolean _estaVigente;
        private TipoContrato _tipo;
        private String _observacion;
        private int _cantidadAsistentes;
        private int _cantidadPersonal;
        #endregion Campos

        #region Propiedades
        public int NumeroContrato { get; set; }
        public Cliente Cliente { get; set; }
        public DateTime Creacion { get; set; }
        public DateTime Termino { get; set; }
        public String FechaHoraInicio { get; set; }
        public String FechaHoraTermino { get; set; }
        public string Direccion { get; set; }
        public Boolean EstaVigente { get; set; }
        public TipoContrato Tipo { get; set; }
        public string Observacion { get; set; }
        public int CantidadAsistentes { get; set; }
        public int CantidadPersonal { get; set; }
        #endregion Propiedades

        #region Constructores
        public Contrato()
        {
            this.Init();
        }

        private void Init()
        {
            this._numeroContrato = 0;
            this._cliente = new Cliente();
            this._creacion = new DateTime(2019, 04, 22);
            this._termino = new DateTime(2019, 04, 22);
            this._fechaHoraInicio = "";
            this._fechaHoraTermino = "";
            this._direccion = "";
            this._estaVigente = false;
            this._tipo = 0;
            this._observacion = "";
            this._cantidadAsistentes = 0;
            this._cantidadPersonal = 0;

        }

        public Contrato(int numeroContrato, Cliente cliente, DateTime creacion, DateTime termino, String fechaHoraInicio, String fechaHoraTermino, string direccion, Boolean estaVigente, TipoContrato tipo, string observacion, int cantidadAsistentes, int cantidadPersonal)
        {
            this.NumeroContrato = numeroContrato;
            this.Cliente = cliente;
            this.Creacion = creacion;
            this.Termino = termino;
            this.FechaHoraInicio = fechaHoraInicio;
            this.FechaHoraTermino = fechaHoraTermino;
            this.Direccion = direccion;
            this.EstaVigente = estaVigente;
            this.Tipo = tipo;
            this.Observacion = observacion;
            this.CantidadAsistentes = cantidadAsistentes;
            this.CantidadPersonal = cantidadPersonal;

        }

        #endregion Constructores


    }
}
