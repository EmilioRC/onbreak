﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnBreak.Modelo
{
    public enum ActividadEmpresa
    {
        // Crear las enumeraciones:
        // Agropecuaria, Mineria, Manufactura, Comercio, Hoteleria, Alimentos, Transporte, Servicios

        Agropecuaria = 0,
        Mineria = 1,
        Manufactura = 2,
        Comercio = 3,
        Hoteleria = 4,
        Alimentos = 5,
        Transporte = 6,
        Servicios = 7

    }

    public enum TipoEmpresa
    {
        // Crear las enumeraciones:
        // SPA, EIRL, Limitada, SociedadAnonima

        SPA = 0,
        EIRL = 1,
        Limitada = 2,
        SociedadAnonima = 3
    }

    public enum TipoContrato
    {
        Aniversario = 0,
        PaseoFinAño = 1,
        Seminario = 2
    }
}
