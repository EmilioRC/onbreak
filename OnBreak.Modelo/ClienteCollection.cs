﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnBreak.Modelo
{
    public class ClienteCollection : List<Cliente>
    {
        #region constructores
        public ClienteCollection()
        {

        }

        public ClienteCollection(Cliente cliente)
        {
            this.Add(cliente);
        }
        #endregion

        #region Metodos
        public Cliente BuscarPorRut(String rut)
        {
            return this.Where(r => r.Rut == rut).FirstOrDefault();
        }

        public ClienteCollection BuscarPorRazonSocial(String razonSocial)
        {
            // Implementar...
            razonSocial = razonSocial.ToUpper();

            List<Cliente> lista = this.Where(r => r.RazonSocial.ToUpper().Contains(razonSocial)).ToList();

            ClienteCollection clientes = new ClienteCollection();

            foreach (Cliente cli in lista)
            {
                clientes.Add(cli);
            }
            if (clientes.Count == 0) return null;
            return clientes;
        }

        public ClienteCollection BuscarActividad(String actividad)
        {
            actividad = actividad.ToUpper();
            List<Cliente> lista = this.Where(r => r.Actividad.ToString().ToUpper().Contains(actividad)).ToList();

            ClienteCollection clientes = new ClienteCollection();

            foreach (Cliente cli in lista)
            {
                clientes.Add(cli);
            }
            if (clientes.Count == 0) return null;
            return clientes;
        }

        public bool EliminarCliente(String rut)
        {
            Cliente cli = BuscarPorRut(rut);
            if (cli == null) return false;
            this.Remove(cli);
            return true;
        }

        public ClienteCollection ObtenerClon()
        {
            // Implementar...
            ClienteCollection clonClientes = new ClienteCollection();

            foreach (Cliente cli in this)
            {
                Cliente clon = new Cliente(cli.Rut, cli.RazonSocial, cli.Direccion
                    , cli.Telefono, cli.Actividad, cli.Tipo);
                clonClientes.Add(clon);
            }
            return clonClientes;
        }
        #endregion
    }
}