﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using OnBreak.Modelo;
using MahApps.Metro.Controls;

namespace OnBreak.Presentacion
{
    /// <summary>
    /// </summary>
    public partial class VentanaConsultaCliente : MetroWindow
    {
        #region Propiedades
        private ClienteCollection MisClientes { get; set; }

        private ClienteCollection MisClientesEnElDataGrid { get; set; }

        public Cliente ClienteSeleccionado { get; set; }
        #endregion Propiedades

        #region Constructores
        public VentanaConsultaCliente(ClienteCollection misClientes)
        {
            InitializeComponent();

            this.MisClientes = misClientes;
            this.MisClientesEnElDataGrid = new ClienteCollection();
            this.ClienteSeleccionado = null;
        }
        #endregion Constructores

        #region Eventos
        private void BtnBuscar_Click(object sender, RoutedEventArgs e)
        {
            BuscarCliente();
        }

        private void BtnAceptar_Click(object sender, RoutedEventArgs e)
        {
            AceptarCliente();
        }

        private void BtnCancelar_Click(object sender, RoutedEventArgs e)
        {
            CancelarBusqueda();
        }
        #endregion Eventos

        #region Métodos
        private void RefrescarDataGrid()
        {
            dgrClientes.ItemsSource = MisClientesEnElDataGrid;
            dgrClientes.Items.Refresh();
        }

        private void BuscarCliente()
        {
            bool rutFueDigitado = !String.IsNullOrEmpty(txtRut.Text);
            bool razonSocialFueDigitada = !String.IsNullOrEmpty(txtRazonSocial.Text);

            String rut = txtRut.Text;
            String razonSocial = txtRazonSocial.Text;
            String actividad = cmbActividad.Text;

            if (rutFueDigitado)
            {
                MostrarRutEncontradoEnDataGrid(rut);
                return;
            }

            if (razonSocialFueDigitada)
            {
                MostrarClientesEncontradosEnDataGrid(razonSocial);
                return;
            }

            if (cmbActividad.SelectedIndex != -1)
            {
                MostrarClientesEncontradosEnDataGridActividad(actividad);
                return;
            }

            MessageBox.Show("Para realizar una búsqueda debe escribir algún criterio en el formulario");
        }
        
        private void BtnEditarCliente_Click(object sender, RoutedEventArgs e)
        {
            AceptarCliente();
        }

        private void AceptarCliente()
        {
            if (dgrClientes.SelectedValue == null)
            {
                MessageBox.Show("Debe seleccionar un cliente");
                return;
            }
            
            ClienteSeleccionado = (Cliente)dgrClientes.SelectedValue;
            
            
            this.Close();
        }

        private void CancelarBusqueda()
        {
            ClienteSeleccionado = null;
            this.Close();
        }

        private void MostrarRutEncontradoEnDataGrid(String rut)
        {
            Cliente cli = MisClientes.BuscarPorRut(rut);
            
            if (cli == null)
            {
                MessageBox.Show("No se encontró un cliente con el rut " + rut);
                MisClientesEnElDataGrid.Clear();
                RefrescarDataGrid();
                return;
            }

            MisClientesEnElDataGrid.Clear();
            MisClientesEnElDataGrid.Add(cli);
            RefrescarDataGrid();
        }

        private void MostrarClientesEncontradosEnDataGrid(String razonSocial)
        {
            ClienteCollection clientes = MisClientes.BuscarPorRazonSocial(razonSocial);

            if (clientes == null)
            {
                MessageBox.Show("No se encontró un cliente con la Razon Social " + razonSocial);
                MisClientesEnElDataGrid.Clear();
                RefrescarDataGrid();
                return;
            }

            MisClientesEnElDataGrid.Clear();
            MisClientesEnElDataGrid = clientes;
            RefrescarDataGrid();
        }

        private void MostrarClientesEncontradosEnDataGridActividad(String actividad)
        {
            ClienteCollection clientes = MisClientes.BuscarActividad(actividad);

            if (clientes == null)
            {
                MessageBox.Show("No se encontró un cliente con la Actividad " + actividad);
                MisClientesEnElDataGrid.Clear();
                RefrescarDataGrid();
                return;
            }

            MisClientesEnElDataGrid.Clear();
            MisClientesEnElDataGrid = clientes;
            RefrescarDataGrid();
        }

        #endregion Métodos
    }
}