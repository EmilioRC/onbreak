﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnBreak.Modelo
{
    public class Cliente
    {
        #region Campos
        private String _rut;
        private String _razonSocial;
        private String _direccion;
        private String _telefono;
        private ActividadEmpresa _actividad;
        private TipoEmpresa _tipo;
        #endregion Campos

        #region Propiedades
        // Codifique aqui las propiedades, utilizando los campos de la clase. Agregue las validaciones indicadas

        public String Rut // Depure el programa, para validar cada una de las siguientes condiciones del Rut. Corrija, si hubiera algún error
        {
            get
            {
                return _rut;
            }
            set
            {
                if (String.IsNullOrEmpty(value)) throw new Exception("El rut no puede estar vacío");

                if (value.Length < 3)
                    throw new Exception("El rut tiene que tener un mínimo de 3 caracteres (por ejemplo: 1-K)");

                if (value.Length > 11)
                    throw new Exception("El rut tiene que tener un máximo de 11 caracteres (por ejemplo 111222333-K)");

                if (value.EndsWith("-k"))
                    throw new Exception("Para los Ruts terminados en K, la letra \"K\" debe estar en mayúscula");

                String dv = value.Substring(value.Length - 2);
                bool dvCorrecto = "-0-1-2-3-4-5-6-7-8-9-K".Contains(dv);

                if (!dvCorrecto)
                    throw new Exception("El rut tiene que tener un dígito verificador, por lo cual debe terminar "
                        + "en guión y un número (por ejemplo \"-3\") o guión y K (por ejemplo \"-K\"");

                String parteNumerica = value.Substring(0, value.Length - 2);
                int num = 0;
                if (!int.TryParse(parteNumerica, out num))
                    throw new Exception("La primera parte del rut, previa al dígito verificador, "
                        + "debe ser un número entre 1 y 999.999.999");

                if (!ValidarRut(value))
                    throw new Exception("El rut no es válido, verifique que lo haya escrito correctamente, y "
                        + "que el dígito verificador es el correcto");

                _rut = value;
            }
        }

        public String RazonSocial
        {
            get
            {
                return _razonSocial;
            }    
            set
            {
                if (value == "")
                    throw new Exception("La razón no puede estar vacia");

                if (value.Length>100)
                    throw new Exception("La razón social es demasiado larga");

                _razonSocial = value;
            }
        } // Validar que: (1) no sea vacio, (2) No supere los 100 caracteres

        public String Direccion
        {
            get
            {
                return _direccion;
            }
            set
            {
                if (value == "")
                    throw new Exception("La dirección no puede estar vacia");

                if (value.Length > 200)
                    throw new Exception("La dirección es demasiado larga");

                _direccion = value;
            }
        } // Validar que: (1) no sea vacio, (2) No supere los 200 caracteres

        public String Telefono {
            get
            {
                return _telefono;
            }
            set
            {
                if (value == "")
                    throw new Exception("El teléfono no puede estar vacia");

                if (value.Length > 15)
                    throw new Exception("El telefono es demasiado largo");

                _telefono = value;
            }
        } // Validar que: (1) no sea vacio, (2) Tenga el formato: +AA BBBB CCCC (por ejemplo: +56 9 4811 1132)

        public ActividadEmpresa Actividad { get; set; } // No necesita validarse, pues la enumeración lo hace por sí misma

        public TipoEmpresa Tipo { get; set; } // No necesita validarse, pues la enumeración lo hace por sí misma

        #endregion Propiedades

        #region Constructores
        public Cliente() // Constructor por defecto
        {
            this.Init();
        }

        private void Init() // Si programó correctamente las propiedades, el siguiente código debería funcionar sin problemas
        {
            this._rut = string.Empty;
            this._razonSocial = string.Empty;
            this._direccion = string.Empty;
            this._telefono = string.Empty;
            this._actividad = ActividadEmpresa.Comercio;
            this._tipo = TipoEmpresa.SPA;
        }

        public Cliente(String rut, String razonSocial, String direccion, String telefono, ActividadEmpresa actividad, TipoEmpresa tipo)
        {
            this.Rut = rut;
            this.RazonSocial = razonSocial;
            this.Direccion = direccion;
            this.Telefono = telefono;
            this.Actividad = actividad;
            this.Tipo = tipo;
        }
        #endregion Constructores

        #region Métodos
        protected bool ValidarRut(string rut)
        {
            bool validacion = false;
            try
            {
                rut = rut.ToUpper();
                rut = rut.Replace(".", "");
                rut = rut.Replace("-", "");
                int rutAux = int.Parse(rut.Substring(0, rut.Length - 1));

                char dv = char.Parse(rut.Substring(rut.Length - 1, 1));

                int m = 0, s = 1;
                for (; rutAux != 0; rutAux /= 10)
                {
                    s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
                }
                if (dv == (char)(s != 0 ? s + 47 : 75))
                {
                    validacion = true;
                }
            }
            catch (Exception)
            {
            }
            return validacion;
        }
        #endregion Métodos
    }
}