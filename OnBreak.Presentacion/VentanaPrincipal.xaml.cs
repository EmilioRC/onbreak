﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using OnBreak.Modelo;
using MahApps.Metro.Controls;

namespace OnBreak.Presentacion
{
    /// <summary>
    /// </summary>
    public partial class VentanaPrincipal : MetroWindow
    {
        protected ClienteCollection TusClientes { get; set; }
        protected ContratoCollection TusContratos { get; set; }

        public VentanaPrincipal()
        {
            InitializeComponent();

            TusClientes = new ClienteCollection();

            TusClientes.Add(new Cliente("21433671-5", "Coca-Cola Company", "San Vicente 123, La Florida", "+56 9 2134 2343", ActividadEmpresa.Comercio, TipoEmpresa.SPA));
            TusClientes.Add(new Cliente("23416800-2", "Zapatillas Monstruo", "Pedro Aguirre Cerda 2536, Estación Central", "+56 9 2134 2343", ActividadEmpresa.Comercio, TipoEmpresa.SPA));
            TusClientes.Add(new Cliente("13534315-3", "Energética Monster", "Alameda 2121, Santiago", "+56 9 2134 2343", ActividadEmpresa.Comercio, TipoEmpresa.SPA));
            TusClientes.Add(new Cliente("11470332-K", "Compañía Zapatillas Puma", "Calle Los Alerces 637, La Reina", "+56 9 2134 2343", ActividadEmpresa.Comercio, TipoEmpresa.SPA));

            TusContratos = new ContratoCollection();

            TusContratos.Add(new Contrato(123, new Cliente("21433671-5", "Coca-Cola Company", "San Vicente 123, La Florida", "+56 9 2134 2343", ActividadEmpresa.Comercio, TipoEmpresa.SPA)
                , new DateTime(1950, 12, 03), new DateTime(1950, 12, 03), "13:04:09", "15:04:10", "Los Heroes", true, TipoContrato.Aniversario, "Holi", 10, 15));

        }

        private void BtnAdministracionContratos_Click(object sender, RoutedEventArgs e)
        {
            VentanaAdmContratos win = new VentanaAdmContratos();
            win.ShowDialog();
        }

        private void BtnListadoContratos_Click(object sender, RoutedEventArgs e)
        {
            VentanaListarContratos win = new VentanaListarContratos(TusContratos);
            win.ShowDialog();
        }

        private void btnAdministrarClientes_Click(object sender, RoutedEventArgs e)
        {
            VentanaRegistroCliente win = new VentanaRegistroCliente();
            win.ShowDialog();
        }

        private void btnListarClientes_Click(object sender, RoutedEventArgs e)
        {
            VentanaConsultaCliente win = new VentanaConsultaCliente(TusClientes);
            win.ShowDialog();
        }
    }
}